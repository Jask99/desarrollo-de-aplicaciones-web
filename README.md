**Desarrollo de Aplicaciones Web con conexiones a base de datos**
**Alumno:**
*Jose Luis Lopez Guzman*
**Semestre**
*5AVP*

-Práctica #1 - 02/09/2022 - Práctica Ejemplo
commit: f3c140de7dc4549a1a1cce180002647220b122c2
Archivo: https://gitlab.com/Jask99/desarrollo-de-aplicaciones-web/-/blob/main/parcial1/practica_ejemplo.html

 -Práctica #2 09/09/2022   - Práctica JavaScript
commit: 85fc0eff863321be966d9ee1bd0b6a1b443d19c6
Archivo: https://gitlab.com/Jask99/desarrollo-de-aplicaciones-web/-/blob/main/parcial1/practicaJavaScript.html

-Práctica #3 - 15/09/2022 - Práctica Web con base de datos - Parte 1
commit: eadf7ce973a6dd35345b6efd5ffe9dc8edf331d8
Archivo: https://gitlab.com/Jask99/desarrollo-de-aplicaciones-web/-/blob/main/parcial1/PracticaWebApp.rar

-Práctica #4 -19/09/2022 - Consulatar datos
commit: c5273182c563a74a9ab841a036751935233afab6
Archivo: https://gitlab.com/Jask99/desarrollo-de-aplicaciones-web/-/blob/main/parcial1/consultarDatos.php

-Práctica #5 - 22/09/2022 - Práctica Web con base datos - registro de datos
commit: 37caecb5664d9b8c87f578d2ad49492bdc83a2be
Archivo: https://gitlab.com/Jask99/desarrollo-de-aplicaciones-web/-/blob/main/parcial1/Practica_web_datos.rar

-Práctica #6 - 26/09/2022 - conexiones
commit: 58914e0ebb475f9c295095d52c4f575ec33020ef
Archivo: https://gitlab.com/Jask99/desarrollo-de-aplicaciones-web/-/blob/main/parcial1/conexion.rar
